/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'Magento_Ui/js/grid/columns/column'
], function (_, Element, columnStatusValidator) {
    'use strict';

    return Element.extend({
        defaults: {
            bodyTmpl: 'Magento_Movie/rating',
            indexField: 'movie_id',
        },

        /**
         * Find image by code in scope of images
         *
         * @param {Object} images
         * @returns {*|T}
         */
        getPercent: function (movie) {
            return movie().rating * 20;
        }

    });
});
