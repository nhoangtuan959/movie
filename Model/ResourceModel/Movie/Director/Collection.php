<?php
namespace Magento\Movie\Model\ResourceModel\Movie\Director;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
	protected $_idFieldName = 'director_id';
	/**
	 * Initialize resource collection
	 *
	 * @return void
	 */
	public function _construct() {
		$this->_init('Magento\Movie\Model\Director', 'Magento\Movie\Model\ResourceModel\Director');
	}
}