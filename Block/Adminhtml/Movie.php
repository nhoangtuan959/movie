<?php
namespace Magento\Movie\Block\Adminhtml;
class Movie extends \Magento\Backend\Block\Widget\Grid\Container {
	protected function _construct() {
		$this->_blockGroup = 'Magento_Movie';
		$this->_controller = 'adminhtml_movie';
		parent::_construct();
	}
}