<?php
namespace Magento\Movie\Controller\Adminhtml\Movie;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
class InlineEdit extends \Magento\Backend\App\Action
{
    /** @var JsonFactory  */
    protected $jsonFactory;
    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
    }
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
//        var_dump(1);die;

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $movieId) {
                    /** @var \Magento\Movie\Model\Movie $model */
                    $model = $this->_objectManager->create('Magento\Movie\Model\Movie');
                    $model->load($movieId);
                    try {
                        $model->setData(array_merge($model->getData(), $postItems[$movieId]));
                        $model->save();
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithMovieId(
                            $model,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
    protected function getErrorWithMovieId(\Magento\Movie\Model\Movie $movie, $errorText)
    {
        return '[Movie ID: ' . $movie->getId() . '] ' . $errorText;
    }

}